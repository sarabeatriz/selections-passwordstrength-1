#include "psfunctions.h"

/// \file

///
/// \fn unsigned int countCharsOfType(const string &st, int (* filterFunction)(int args) , int fromIdx = 0, int toIdx = -1)
/// \~English
/// \brief Given a string will return the number of characters
/// of a certain type.
/// \param st string to analyze
/// \param filterFunction a function pointer to a function that returns 1
/// when a character of the type is given. For example, if function is islower,
/// then countCharsOfType will count lowercase characters.
/// \param fromIdx index from where to start count
/// \param toIdx last index for count
/// \~Spanish
/// \brief Dada una cadena de caracters devuelve el numero de caracteres de un
/// cierto tipo.
/// \param st Cadena de caracteres a analizar
/// \param filterFunction Apuntador a funcion a una funcion que devuelve 1
/// cuando un caracter de el typo es dado.  Por ejemplo, si la funciones la funcion
/// islower, entonces countCharsOfType contara caracteres en minuscula
/// \param fromIdx indice de desde donde comenzar a contar.
/// \param toIdx ultimo indice para contar.
unsigned int countCharsOfType(const string &st,
    int (* filterFunction)(int args) ,
    int fromIdx = 0, int toIdx = -1) {

    unsigned int ctr = 0;
    unsigned int len = st.length();

    // set the last index, -1 is the default value,
    // which will be interpreted as the last index.
    if (toIdx == -1) toIdx = len - 1;

    //cout << "fromIdx: " << fromIdx << " toIdx:" << toIdx << endl;

    // if length or indices are invalid return a 0
    if ( len == 0 || fromIdx < 0 || fromIdx > static_cast<int>(len - 1) ||
         fromIdx > toIdx || toIdx < 0 || toIdx > static_cast<int>(len - 1) )
        return 0;

    for(int i = fromIdx; i <= toIdx; i++) {
        if ( filterFunction( st[i]) )
            ctr++;
    }
    return ctr;
}

/// \fn unsigned int countUppercase(const string &st)
/// \~English
/// \brief Given a string will return the number of uppercase
/// characters.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Dada una cadena de caracteres devolvera el numero de caracteres
/// en mayuscula.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int countUppercase(const string &st) {
    return countCharsOfType(st, isupper);
}

/// \fn unsigned int countLowercase(const string &st)
/// \~English
/// \brief Given a string will return the number of lowercase
/// characters.
/// \param st  string by reference to analyze
/// \~Spanish
/// \brief Dada una cadena de caracteres devolvera el numero de caracteres
/// en minuscula.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int countLowercase(const string &st) {
    return countCharsOfType(st, islower);
}

/// \fn int isSymbol(int c)
/// \~English
/// \brief Returns 1 if the passed argument is a symbol.
/// \param c  the character to be analyzed.
/// \~Spanish
/// \brief Devuelve 1 si el argumento es un simbolo.
/// \param c el caracter a ser analizado.
int isSymbol(int c) {
    return ( !islower(c) && !isupper(c) && !isdigit(c) );
}


/// \fn int isDigitOrSymbol(int c)
/// \~English
/// \brief Returns 1 if the passed argument is a digit or symbol.
/// \param c  the character to be analyzed.
/// \~Spanish
/// \brief Devuelve 1 si el argumento es un digito o simbolo.
/// \param c el caracter a ser analizado.
int isDigitOrSymbol(int c) {
    return ( isdigit(c) || isSymbol(c) );
}


/// \fn unsigned int countDigits(const string &st)
/// \~English
/// \brief Given a string will return the number of digits.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Dada una cadena de caracteres devolvera el numero de digitos.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int countDigits(const string &st) {
    return countCharsOfType(st, isdigit);
}


/// \fn unsigned int countSymbols(const string &st)
/// \~English
/// \brief Given a string will return the number of symbols.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Dada una cadena de caracteres devolvera el numero de simbolos.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int countSymbols(const string &st) {
    return countCharsOfType(st, isSymbol);
}

/// \fn string toUpperString(const string &st)
/// \~English
/// \brief Returns an uppercase version of the received string.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Devuelve una version en mayusculas de la cadena de caracteres recibida.
/// \param st cadena de caracteres por referencia a analizar.
string toUpperString(const string &st) {
    string res = "";
    for (unsigned int i = 0; i < st.length(); i++)
        res.push_back(toupper(st[i]));
    return res;
}


/// \fn unsigned int middleDigitsOrSymbols(const string &st)
/// \~English
/// \brief Returns the number of digits and symbols
/// that are not the first or last characters of the received string.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Devuelve el numero de digitos y simbolos que no son el primer
/// o el ultimo caracter de la cadena recibida.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int middleDigitsOrSymbols(const string &st) {
    return countCharsOfType(st, isDigitOrSymbol, 1, st.length() - 2);
}

///
/// \fn unsigned int countCharsOfType(const string &st, int (* filterFunction)(int args) , int fromIdx = 0, int toIdx = -1)
/// \~English
/// \brief Given a string will return the number of characters
/// of a certain type that follow a character of that same type.
/// \param st string to analyze
/// \param filterFunction a function pointer to a function that returns 1
/// when a character of the type is given. For example, if function is islower,
/// then countConsecutive will count consecutive lowercase characters.
/// \~Spanish
/// \brief Dada una cadena de caracters devuelve el numero de caracteres de un
/// cierto tipo que sigue un caracter de el mismo tipo.
/// \param st Cadena de caracteres a analizar
/// \param filterFunction Apuntador a funcion a una funcion que devuelve 1
/// cuando un caracter de el typo es dado.  Por ejemplo, si la funciones la funcion
/// islower, entonces countConsecutive contara caracteres consecutivos en minuscula
unsigned int countConsecutive(const string &st,
    int (* filterFunction)(int args) ) {

    // prevSameType will be made true whenever we find a caracter
    // of the type we are looking.
    bool prevSameType = false;
    unsigned int ctr  = 0;

    for(unsigned int i = 0; i < st.length(); i++) {
        if ( filterFunction( st[i]) ) {
            if (prevSameType) ctr++;
            prevSameType = true;
        }
        else
            prevSameType = false;
    }
    return ctr;
}

/// \fn unsigned int consecUppercase(const string &st)
/// \~English
/// \brief Given a string will return the number of
/// uppercase characters that follow a character of that same type.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Dada una cadena devolvera el numero de caracteres en mayuscula que
/// sigue a un caracter del mismo tipo.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int consecUppercase(const string &st) {
    return countConsecutive(st, isupper);
}


/// \fn unsigned int consecLowercase(const string &st)
/// \~English
/// \brief Given a string will return the number of
/// lowercase characters that follow a character of that same type.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Dada una cadena devolvera el numero de caracteres en minuscula que
/// sigue a un caracter del mismo tipo.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int consecLowercase(const string &st) {
    return countConsecutive(st, islower);
}


/// \fn unsigned int consecDigits(const string &st)
/// \~English
/// \brief Given a string will return the number of
/// digits that follow a digit.
/// \param st string by reference to analyze
/// \~Spanish
/// \brief Dada una cadena devolvera el numero de digitos que
/// sigue a un digito.
/// \param st cadena de caracteres por referencia a analizar.
unsigned int consecDigits(const string &st) {
    return countConsecutive(st, isdigit);
}

