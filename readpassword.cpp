/*
RAN 2014-08-20
-- Simplified the upperCheck, lowerCheck, etc.. repeatChars, functions
-- Fixed many of the warnings
-- Eliminated findLetterpos, alpha[]

RAN 2014-08-21
-- Las funciones de contar consecutivos tienen error.  Estoy arreglando.
-- Cree una nueva función "generica" para contar consectuivos: countRepeated

IVE 2015-06-14
-- declare variables digit_count y symbol_count que estaban sin declarar
*/



/// \file

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "psfunctions.h"
#include <vector>
#include <QDebug>
#include <string>
using namespace std;

/// \fn void MainWindow::readPass(const string &pass)
/// \~English
/// \brief Main function, where all the other PasswordStrength
/// functions are executed
/// \param pass string by reference to analyze
/// \~Spanish
/// \brief Funcion principal, donde todas las otras funciones de fortaleza
/// de passwords son ejecutadas.
/// \param pass cadena de caracteres por referencia a analizar.
void MainWindow::readPass(const string &pass){
    string strength = "Compute Me" ;
    int totalScore = 0;
    int requirements = 0;
    int count ;
    int score ;
    int len ;
    
    /*
     * Password Additions:
     */
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // Check the length of the string (min:8, better>8, n*4)
       len = count = pass.length() ;
       score = 4 * count ;
       setNumberOfCharacters(count, score);
       totalScore += score ;

    // Uppercase letters (good-min:1, better-min:2, (len-n)*2)
       count = countUppercase(pass) ;
       score=0;
       if(count) {
           requirements += 1;
           score = (len - count) * 2 ;
        }
       setUpperCharacters(count, score);
       totalScore += score ;



    // Lowercase letters (good-min:1, better-min:2, (len-n)*2)
    
    // YOUR CODE HERE
    
    // Digits (good-min:1, better-min:2, n*4)
    
    // YOUR CODE HERE

    // Symbols (good-min:1, better-min:2, n*6)
    
    // YOUR CODE HERE

    // Middle Numbers or Symbols (good-min:1, better-min:2, n*2)
    
    // YOUR CODE HERE

    // Add requirements score
    
    // YOUR CODE HERE
    
    // Letters Only [-n]
    
    // YOUR CODE HERE
    
    // Digits Only [-n]
    
    // YOUR CODE HERE

    // Consecutive Uppercase Letters [-(n*2)]
    
    // YOUR CODE HERE


    // Consecutive Lowercase Letters [-(n*2)]
    
    // YOUR CODE HERE

    // Consecutive Digits [-(n*2)]
    
    // YOUR CODE HERE


  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//

    // Clasify the strength to control progress bar
    
    
    // YOUR CODE HERE
 

        strengthDisplay(strength, totalScore);

}

