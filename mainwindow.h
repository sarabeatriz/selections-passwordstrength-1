#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <vector>
#include <QMainWindow>
#include <string>
#include <QLabel>
#include <QCheckBox>

using namespace std;

///
/// This class contains all the fucntions necessary to run
/// the PasswordStrength lab. 
///

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    vector<char> V;

    /// \fn void MainWindow::readPass(const string &pass)
    /// \~English
    /// \brief Main function, where all the other PasswordStrength
    /// functions are executed
    /// \param pass string by reference to analyze
    /// \~Spanish
    /// \brief Funcion principal, donde todas las otras funciones de fortaleza
    /// de passwords son ejecutadas.
    /// \param pass cadena de caracteres por referencia a analizar.
    void readPass(const string &pass);

    /// \fn void MainWindow::strengthDisplay(string strength, int totalScore)
    /// \~English
    /// \brief Function to set the computed strength and total score password
    /// strength in the GUI.
    /// \param strength Computed strenght of the password.
    /// \param totalScore Total score of the strength of the password.
    /// \~Spanish
    /// \brief Funcion para establecer la fuerza y la puntuacion total calculada de la contrasena.
    /// \param strength Fuerza computada de la contrasena.
    /// \param totalScore Puntuacion total de la fuerza de la contrasena.
    void strengthDisplay(string strength, int totalScore);

    /// \fn void MainWindow::setNumberOfCharacters(int count, int score)
    /// \~English
    /// \brief Function to set the number of characters and score in the GUI
    /// \param count the number of characters found.
    /// \param score the score given by the count of characters found.
    /// \~Spanish
    /// \brief Funcion para establecer el number de caracteres y la puntuacion en el GUI
    /// \param count el numero de letras encontrado.
    void setNumberOfCharacters(int count, int score) ;

    /// \fn void MainWindow::setUpperCharacters(int count, int score)
    /// \~English
    /// \brief Function to set the number of upper case characters and score in the GUI
    /// \param count the number of upper case characters  found.
    /// \param score the score given by the count of upper case characters.
    /// \~Spanish
    /// \brief Funcion para establecer el number de caracteres mayusculas y la puntuacion en el GUI
    /// \param count el numero de letras mayusculas encontrado.
    /// \param score le puntuacion dada por la cuenta de letras mayusculas encontrada.
    void setUpperCharacters(int count, int score) ;

    /// \fn void MainWindow::setLowerCharacters(int count, int score)
    /// \~English
    /// \brief Function to set the number of lower case characters and score in the GUI
    /// \param count the number of lower case characters  found.
    /// \param score the score given by the count of lower case characters.
    /// \~Spanish
    /// \brief Funcion para establecer el number de caracteres minusculas y la puntuacion en el GUI
    /// \param count el numero de letras minusculas encontrado.
    /// \param score le puntuacion dada por la cuenta de letras minusculas encontrada.
    void setLowerCharacters(int count, int score) ;

    /// \fn void MainWindow::setDigits(int count, int score)
    /// \~English
    /// \brief Function to set the number of digits characters and score in the GUI
    /// \param count the number of digits  found.
    /// \param score the score given by the count of digit characters.
    /// \~Spanish
    /// \brief Funcion para establecer el number de digitos y la puntuacion en el GUI
    /// \param count el numero de digitos encontrado.
    /// \param score le puntuacion dada por la cuenta de digitos encontrada.
    void setDigits(int count, int score) ;

    /// \fn void MainWindow::setSymbols(int count, int score)
    /// \~English
    /// \brief Function to set the number of symbols characters and score in the GUI
    /// \param count the number of symbols  found.
    /// \param score the score given by the count of symbols.
    /// \~Spanish
    /// \brief Funcion para establecer el number de simbolos y la puntuacion en el GUI
    /// \param count el numero de simbolos encontrado.
    /// \param score le puntuacion dada por la cuenta de simbolos encontrada.
    void setSymbols(int count, int score) ;

    /// \fn void MainWindow::setMiddleDigitsOrSymbols(int count, int score)
    /// \~English
    /// \brief Function to set the number of middle digits or symbols and score in the GUI
    /// \param count the number of middle digits or symbols  found.
    /// \param score the score given by the count of middle digits or symbols.
    /// \~Spanish
    /// \brief Funcion para establecer el number de simbolos o digitos entre medio y la puntuacion en el GUI
    /// \param count el numero de simbolos y digitos entre medio encontrado.
    /// \param score le puntuacion dada por la cuenta de simbolos y digitos entre medio encontrada.
    void setMiddleDigitsOrSymbols(int count, int score) ;

    /// \fn void MainWindow::setRequirements(int count, int score)
    /// \~English
    /// \brief Function to set the number password strength requirements met and score in the GUI
    /// \param count the number of requirements met.
    /// \param score the score given by requirements.
    /// \~Spanish
    /// \brief Funcion para establecer el number de requisitos cumplidos para password fuertes
    ///  y la puntuacion en el GUI
    /// \param count el numero de requisitos .
    /// \param score le puntuacion dada por la cuenta requisitos.
    void setRequirements(int count, int score) ;


    /// \fn void MainWindow::setLettersOnly(int count, int score)
    /// \~English
    /// \brief Function to set the number of letters if there were only letters and score in the GUI
    /// \param count the number of letters only found.
    /// \param score the score given by letters only found.
    /// \~Spanish
    /// \brief Funcion para establecer el number de letras si solo habían letras
    ///  y la puntuacion en el GUI
    /// \param count el numero de letras solamente encontradas .
    /// \param score le puntuacion dada por la cuenta solamente de letras encontradas.
    void setLettersOnly(int count, int score) ;

    /// \fn void MainWindow::setDigitsOnly(int count, int score)
    /// \~English
    /// \brief Function to set the number of digits if there were only digits and score in the GUI
    /// \param count the number of digits only found.
    /// \param score the score given by digits only found.
    /// \~Spanish
    /// \brief Funcion para establecer el number de digitos si solo habían digitos
    ///  y la puntuacion en el GUI
    /// \param count el numero de digitos solamente encontradas .
    /// \param score le puntuacion dada por la cuenta solamente de digitos encontradas.
    void setDigitsOnly(int count, int score) ;

    /// \fn void MainWindow::setConsecutiveUpper(int count, int score)
    /// \~English
    /// \brief Function to set the number of consecutive upper characters and score in the GUI
    /// \param count the number of consecutive upper characters found.
    /// \param score the score given by the count of consecutive upper characters found.
    /// \~Spanish
    /// \brief Funcion para establecer el number de letras mayusculas consecutivas y la puntuacion en el GUI
    /// \param count el numero de letras mayusculas consecutivas encontrado.
    /// \param score le puntuacion dada por la cuenta de las letras mayusculas consecutivas.
    void setConsecutiveUpper(int count, int score) ;

    /// \fn void MainWindow::setConsecutiveLower(int count, int score)
    /// \~English
    /// \brief Function to set the number of consecutive lower characters and score in the GUI
    /// \param count the number of consecutive lower characters found.
    /// \param score the score given by the count of consecutive lower characters found.
    /// \~Spanish
    /// \brief Funcion para establecer el number de letras minusculas consecutivas y la puntuacion en el GUI
    /// \param count el numero de letras minusculas consecutivas encontrado.
    /// \param score le puntuacion dada por la cuenta de las letras minusculas consecutivas.
    void setConsecutiveLower(int count, int score) ;

    /// \fn void MainWindow::setConsecutiveDigits(int count, int score)
    /// \~English
    /// \brief Function to set the number of consecutive digits and score in the GUI
    /// \param count the number of consecutive digits found.
    /// \param score the score given by the count of consecutive digits found.
    /// \~Spanish
    /// \brief Funcion para establecer el number de digitos consecutivos y la puntuacion en el GUI
    /// \param count el numero de digitos consecutivos encontrado.
    /// \param score le puntuacion dada por la cuenta de los digitos consecutivos.
    void setConsecutiveDigits(int count, int score) ;



private slots:
    void on_lineEdit_textChanged(const QString &arg1);

    void on_HiddenCheckBox_clicked(bool checked);


private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
